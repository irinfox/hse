__author__ = 'irinfox'
import tkinter as tk # help (tk), tk.Tk(), ...

#примитивные рисовашки
root = tk.Tk()
canvas = tk.Canvas(root, width=500, height=500, background="white")

canvas.pack() #расположить
canvas.create_oval(10, 10, 200, 200, fill="#DEB887")
canvas.create_line(10, 10, 200, 200, fill="#FF4500", width=10)
canvas.create_polygon([50, 100], [150, 200], [100, 300], width=10, fill="blue")
canvas.create_text(200, 50, text="Hello, HSE", font="Arial 20", anchor="nw") #якорь - привязка координаты, nw - северо-запад
#т.е. точка 200, 50 является левым верхним углом прямоугольника, в который вписан текст
root.mainloop()
