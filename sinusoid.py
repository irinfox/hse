import math

step = 0.5
scale = 5
offset = 6

def print_row(x):
    print("*" * int(scale * math.sin(j * step) + offset))


for j in range(0, 10):
	print_row(j)
